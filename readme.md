# Additive Markov chain java Algorithm

https://en.wikipedia.org/wiki/Additive_Markov_chain


## Installation
To compile: mvn clean compile assembly:single

To Run: java -jar target/MarkovChain-1.0-SNAPSHOT-jar-with-dependencies.jar input.txt output.txt 3 5000
## Usage
input.txt - name of the text file in UTF-8 encoding that will be used to build dictionary. Only Russian text supported;

output.txt - name of the text file for result of text generation;

3 - length of "chain memory" that will be used to select new word;

5000 - output word count.
