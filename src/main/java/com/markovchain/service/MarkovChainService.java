package com.markovchain.service;


import com.google.common.collect.Sets;
import com.markovchain.model.api.ProbabilitiesGraph;
import com.markovchain.model.impl.probability.ProbabilityGraph;
import com.markovchain.utils.FileReader;
import com.markovchain.utils.FileWriter;
import com.markovchain.utils.Randomizer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;


/* Класс генерирует текст учитывая статистику построенную по входному файлу.
 * Основная идея алгоритма:
 * Строится список смежности входного текста (graph.nodes), все рёбра нумеруются (BasicEdgeProperty.serialNumbers) и сохраняются.
 * Генерится первоначальный набор слов effectiveListString длины effectiveStateCount на основе глобальной встречаемости слов и в дальнейшем
 * стараемся продолжить эту последовательность, учитывая опыт из входного текста.
 * Нумерация рёбер используется для определения корректной последовательности переходов между вершинами.
 * Например, если из ААА -> БББ можно попасть по ребрам (1, 4), а из БББ -> ГГГ только по ребру (5),
 * то AAA -> ГГГ можно попасть только по ребру (4). Работа с этими множествами в итоге самая трудозатратная, постоянно инкрементируется
 * первое множество и пересекается со вторым effectiveStateCount раз, а колличество рёбер огромно, нужно оптимизировать хотябы работу с памятью.
 *
 */
public class MarkovChainService {

    static public final String SEPARATORS = " \r\t";
    static public final String DOT = ".";
    static public final String SPECIAL_WORDS = "," + DOT;
    static public final String ALLOWED_SYMBOLS = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    static public final Charset ENCODING = StandardCharsets.UTF_8;

    public void generateChain(String inputFilePath, String outputFilePath, int effectiveStateCount, int outputWordCount) {
        long startTime = System.currentTimeMillis();
        ProbabilitiesGraph graph = readGraphFromFile(inputFilePath);
        System.out.printf("Dictionary graph was generated for %dms%n", System.currentTimeMillis() - startTime);
        System.out.println("Graph node count: " + graph.getNodes().size());
        System.out.println("Graph total edge count: " + graph.getTotalCount());
        generateChain(graph, outputFilePath, effectiveStateCount, outputWordCount);
        System.out.printf("Chain was generated for %dms%n", System.currentTimeMillis() - startTime);
    }

    private void generateChain(ProbabilitiesGraph graph, String outputFilePath, int effectiveStateCount, int outputWordCount) {
        int countOfWritten = 0;
        LinkedList<String> effectiveListString = new LinkedList<>();
        Map<String, Double> nodeProbabilities = graph.getNodeProbabilities();
        try (FileWriter fileWriter = new FileWriter(outputFilePath, ENCODING)) {
            String newWord = null;
            String previousWord = null;
            while (countOfWritten < outputWordCount) {
                if (effectiveListString.size() < effectiveStateCount) {
                    newWord = generateWordByGlobalProbability(nodeProbabilities);
                    while (SPECIAL_WORDS.contains(newWord) && previousWord != null && SPECIAL_WORDS.contains(previousWord))
                        newWord = generateWordByGlobalProbability(nodeProbabilities); //skip double special symbol
                } else {
                    newWord = generateWordByEffectiveStringAndProbability(graph, effectiveListString, nodeProbabilities);
                    while (SPECIAL_WORDS.contains(newWord) && previousWord != null && SPECIAL_WORDS.contains(previousWord))  //skip double special symbol
                        newWord = generateWordByGlobalProbability(nodeProbabilities);
                    effectiveListString.removeFirst();
                }
                previousWord = writeFormattedWordToFile(newWord, previousWord, fileWriter).equals("") ? previousWord : newWord;
                effectiveListString.addLast(newWord);
                countOfWritten++;
            }
        } catch (IOException e) {
            throw new RuntimeException("Is not possible to write to output file, some error has occurred:", e);
        }
    }

    private String generateWordByEffectiveStringAndProbability(ProbabilitiesGraph graph, List<String> effectiveListString, Map<String, Double> nodeProbabilities) {
        Map<String, Set<Long>> possibleLastNodes;
        Set<Long> suitableSerialNumbers = null;
        Map<String, Integer> finalWordCount = new HashMap<>();
        if (effectiveListString.size() > 1) {
            int i = 1;
            suitableSerialNumbers = increment(graph.getNextNodesWithSerialNumbers(effectiveListString.get(0)).get(effectiveListString.get(1)));
            while (CollectionUtils.isNotEmpty(suitableSerialNumbers) && i < effectiveListString.size() - 1) {
                Set<Long> nextSerialNumbers = graph.getNextNodesWithSerialNumbers(effectiveListString.get(i)).get(effectiveListString.get(i + 1));
                if (nextSerialNumbers == null || nextSerialNumbers.isEmpty())
                    return generateWordByGlobalProbability(nodeProbabilities);
                suitableSerialNumbers.retainAll(nextSerialNumbers);
                suitableSerialNumbers = increment(suitableSerialNumbers);
                i++;
            }
            if (CollectionUtils.isEmpty(suitableSerialNumbers))
                return generateWordByGlobalProbability(nodeProbabilities);
            possibleLastNodes = graph.getNextNodesWithSerialNumbers(effectiveListString.get(i));
            for (Map.Entry<String, Set<Long>> possibleNodeWithSID : possibleLastNodes.entrySet()) {
                Set<Long> possibleEdges = Sets.intersection(suitableSerialNumbers, possibleNodeWithSID.getValue());
                if (CollectionUtils.isNotEmpty(possibleEdges))
                    finalWordCount.put(possibleNodeWithSID.getKey(), possibleEdges.size());
            }
        } else {
            possibleLastNodes = graph.getNextNodesWithSerialNumbers(effectiveListString.get(0));
            for (Map.Entry<String, Set<Long>> possibleNodeWithSID : possibleLastNodes.entrySet()) {
                finalWordCount.put(possibleNodeWithSID.getKey(), possibleNodeWithSID.getValue().size());
            }
        }

        if (finalWordCount.isEmpty()) return generateWordByGlobalProbability(nodeProbabilities);
        return Randomizer.getRandomKeyByProbabilities(calculateProbabilitiesByFrequency(finalWordCount));
    }

    private String generateWordByGlobalProbability(Map<String, Double> wordProbabilities) {
        return Randomizer.getRandomKeyByProbabilities(wordProbabilities);
    }

    private Set<Long> increment(Set<Long> integerSet) {
        if (integerSet == null) return SetUtils.emptySet();
        return integerSet.stream().map(integer -> integer + 1).collect(Collectors.toSet());
    }

    private Map<String, Double> calculateProbabilitiesByFrequency(Map<String, Integer> words2Count) {
        int totalCount = words2Count.values().stream().mapToInt(Integer::intValue).sum();
        return words2Count.entrySet().stream().map(word2Count -> new AbstractMap.SimpleEntry<>(word2Count.getKey(), 1.0 * word2Count.getValue() / totalCount))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }

    public ProbabilitiesGraph readGraphFromFile(String inputFilePath) {
        ProbabilitiesGraph graph = new ProbabilityGraph();
        try (FileReader fileReader = new FileReader((inputFilePath), ENCODING)) {
            int i;
            StringBuilder word = new StringBuilder();
            String previousWord = null;
            while ((i = fileReader.read()) != -1) {
                if (SEPARATORS.indexOf((char) i) != -1 || SPECIAL_WORDS.indexOf((char) i) != -1) {
                    String newWord = word.toString();
                    if (word.length() > 0) {
                        graph.addNode(newWord);
                        if (StringUtils.isNotBlank(previousWord))
                            graph.connectNodes(previousWord, newWord);
                        previousWord = newWord;
                    }
                    word.setLength(0);
                    if (SPECIAL_WORDS.indexOf((char) i) != -1) {
                        newWord = String.valueOf((char) i);
                        graph.addNode(newWord);
                        if (StringUtils.isNotBlank(previousWord)) {
                            graph.connectNodes(previousWord, newWord);
                        }
                        previousWord = newWord;
                    }
                } else {
                    if (ALLOWED_SYMBOLS.indexOf((char) i) != -1)
                        word.append(Character.toLowerCase((char) i));
                }
            }
            if (word.length() != 0 && previousWord != null) { // corner case -> last word
                String newWord = word.toString();
                graph.addNode(newWord);
                graph.connectNodes(previousWord, newWord);
            }

        } catch (IOException e) {
            throw new RuntimeException("Is not possible to read from input file, some error has occurred:", e);
        }
        return graph;
    }

    private String writeFormattedWordToFile(String newWord, String previousWord, FileWriter fileWriter) throws IOException {
        String formattedWord = "";
        if (previousWord != null && !"".equals(previousWord)) {
            if (DOT.equals(previousWord)) {
                if (!SPECIAL_WORDS.contains(newWord)) {
                    formattedWord = " " + newWord.substring(0, 1).toUpperCase() + newWord.substring(1);
                }
            } else if (SPECIAL_WORDS.contains(previousWord)) { //if it is comma
                if (!SPECIAL_WORDS.contains(newWord)) {
                    formattedWord = " " + newWord;
                }
            } else if (SPECIAL_WORDS.contains(newWord))
                formattedWord = newWord;
            else
                formattedWord = " " + newWord;
        } else if (!SPECIAL_WORDS.contains(newWord))
            formattedWord = newWord.substring(0, 1).toUpperCase() + newWord.substring(1);

        if (!"".equals(formattedWord)) {
            fileWriter.write(formattedWord);
        }
        return formattedWord;
    }

}
