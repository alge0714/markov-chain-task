package com.markovchain.utils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class FileWriter implements AutoCloseable {

    BufferedWriter out;

    public FileWriter(String filePath, Charset encoding) throws IOException {
        openStream(filePath, encoding);
    }

    public FileWriter() {
    }

    public void openStream(String filePath, Charset encoding) throws IOException {
        out = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(filePath), encoding));
    }

    public void write(String text) throws IOException {
        out.write(text);
    }

    public void write(char text) throws IOException {
        out.write(text);
    }

    public void closeStream() throws IOException {
        out.close();
    }

    @Override
    public void close() throws IOException {
        closeStream();
    }
}
