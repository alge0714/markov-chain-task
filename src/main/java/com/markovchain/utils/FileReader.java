package com.markovchain.utils;

import java.io.*;
import java.nio.charset.Charset;

public class FileReader implements AutoCloseable{

    BufferedReader in;

    public FileReader(String filePath, Charset encoding) throws IOException {
        openStream( filePath,  encoding);
    }

    public FileReader() {
    }

    public void openStream (String filePath, Charset encoding) throws IOException {
        in = new BufferedReader
                (new InputStreamReader(new FileInputStream(filePath), encoding));
    }

    public int read () throws IOException {
        return in.read();
    }

    public void closeStream () throws IOException {
        in.close();
    }

    @Override
    public void close() throws IOException {
        closeStream ();
    }
}
