package com.markovchain.utils;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class Randomizer {

    //return index of selected probabilities
    static public int getRandomByProbabilities(List<Double> probabilities) {
        Random random = new Random();
        double p = random.nextDouble();
        double sum = 0.0;
        int i = 0;
        while (sum < p) {
            sum += probabilities.get(i);
            i++;
        }
        return i - 1;
    }

    //return key by probabilities
    public static <T> T getRandomKeyByProbabilities(Map<T, Double> keysAndProbabilities) {
        Random random = new Random();
        double p = random.nextDouble();
        double sum = 0.0;
        int i = 0;
        T resultObject = null;
        for (Map.Entry<T, Double> it : keysAndProbabilities.entrySet()) {
            if ((sum < p)) {
                resultObject = it.getKey();
                sum += it.getValue();
            } else break;
        }
        return resultObject;
    }
}
