package com.markovchain.model.impl.basic;

import com.markovchain.model.api.Graph;
import com.markovchain.model.api.NodeProperty;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BasicGraph implements Graph {

    protected Map<String, NodeProperty> nodes = new HashMap<>();
    long edgeTotalCount;

    public Map<String, NodeProperty> getNodes() {
        return nodes;
    }

    //will add "from" node in graph if does not exist
    public void connectNodes(String from, String to) {
        NodeProperty nodeProperties = nodes.get(from);
        if (nodeProperties == null) {
            NodeProperty newBasicNodeProperty = createNewNodeProperty();
            newBasicNodeProperty.putEdge(to, edgeTotalCount + 1L);
            nodes.put(from, newBasicNodeProperty);
        } else nodeProperties.putEdge(to, edgeTotalCount + 1L);
        edgeTotalCount = edgeTotalCount + 1;
    }

    public void addNode(String key) {
        NodeProperty nodeProperties = nodes.get(key);
        if (nodeProperties == null)
            nodes.put(key, createNewNodeProperty());
        else nodeProperties.increaseCount();
    }

    protected NodeProperty createNewNodeProperty() {
        return new BasicNodeProperty();
    }

    //count of not unique nodes
    public int getTotalCount() {
        return nodes.values().stream().map(NodeProperty::getNodeCount).mapToInt(Integer::intValue).sum();
    }

    @Override
    public Map<String, Set<Long>> getNextNodesWithSerialNumbers(String currentWord) {
        NodeProperty nodeProperty = nodes.get(currentWord);
        return nodeProperty.getEdges();
    }

    @Override
    public String toString() {
        return "BasicGraph{" +
                "nodes=" + nodes +
                ", edgeTotalCount=" + edgeTotalCount +
                '}';
    }
}
