package com.markovchain.model.impl.basic;


import com.markovchain.model.api.NodeProperty;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BasicNodeProperty implements NodeProperty {
    //target word (String) -> edge serial number of edges to this node
    protected Map<String, Set<Long>> edges = new HashMap<>();
    int nodeCount;

    public BasicNodeProperty() {
        nodeCount = 1;
    }

    @Override
    public Map<String, Set<Long>> getEdges() {
        return edges;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void increaseCount() {
        nodeCount = nodeCount + 1;
    }

    @Override
    public void putEdge(String value, long serialNumber) {
        Set<Long> edgeProperty = edges.get(value);
        if (edgeProperty == null)
            edges.put(value, createNewEdgeProperty(serialNumber));
        else edgeProperty.add(serialNumber);
    }

    protected Set<Long> createNewEdgeProperty(long serialNumber) {
        return new HashSet<Long>(){{add(serialNumber);}};
    }

    @Override
    public Set<Long> getEdgeProperties(String targetNode) {
        return edges.get(targetNode);

    }

    @Override
    public Set<String> getTargetNodes() {
        return edges.keySet();
    }

    @Override
    public String toString() {
        return "BasicNodeProperty{" +
                "nodeCount=" + nodeCount +
                ", edges=" + edges +
                '}';
    }
}