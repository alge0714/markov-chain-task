package com.markovchain.model.impl.probability;


import com.markovchain.model.api.ProbabilitiesGraph;
import com.markovchain.model.impl.basic.BasicGraph;

import java.util.*;
import java.util.stream.Collectors;

public class ProbabilityGraph extends BasicGraph implements ProbabilitiesGraph {

    @Override
    public Map<String, Double> getNodeProbabilities() {
        int totalCount = getTotalCount();
        return nodes.entrySet().stream().map(stringNodePropertyEntry ->
                new AbstractMap.SimpleEntry<>(stringNodePropertyEntry.getKey(), 1.0 / totalCount * stringNodePropertyEntry.getValue().getNodeCount()))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }



}
