package com.markovchain.model.api;

import java.util.Map;

public interface ProbabilitiesGraph extends Graph {

    Map<String, Double> getNodeProbabilities();

}
