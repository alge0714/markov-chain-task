package com.markovchain.model.api;;

import java.util.Map;
import java.util.Set;

public interface NodeProperty {

    int getNodeCount();

    void increaseCount();

    Set<Long> getEdgeProperties(String targetNode);

    Set<String> getTargetNodes();

    Map<String, Set<Long>> getEdges();

    void putEdge(String value, long serialNumber);
}
