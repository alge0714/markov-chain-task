package com.markovchain.model.api;

import java.util.Map;
import java.util.Set;

public interface Graph {

    void connectNodes(String from, String to);

    void addNode(String key);

    int getTotalCount();

    Map<String, Set<Long>> getNextNodesWithSerialNumbers(String currentWord);

    Map<String, NodeProperty> getNodes();
}
