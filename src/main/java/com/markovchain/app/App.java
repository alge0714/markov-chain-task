package com.markovchain.app;


import com.markovchain.service.MarkovChainService;

public class App
{
    public static void main( String[] args )
    {
        MarkovChainService chainService = new MarkovChainService();
        System.out.println("Program was run with following params:");
        for (String arg : args)
            System.out.println(arg);

        chainService.generateChain(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));

    }
}
