package com.inputFIleGenerator;

import com.markovchain.service.MarkovChainService;
import com.markovchain.utils.FileWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//вообщем, не очень полезный генератор, так как получившийся текст имеет слишком большую связность (каждое слово может следовать за каждым, граф по такому тексту очень плотный)
public class Main {

    public static void main(String[] args) {

        try (FileWriter fileWriter = new FileWriter("input.txt", MarkovChainService.ENCODING)) {
            String pool = MarkovChainService.ALLOWED_SYMBOLS  + "         ";
            char[] specialPool = (MarkovChainService.SEPARATORS + MarkovChainService.SPECIAL_WORDS).toCharArray();
            char[] charPool = pool.toCharArray();
            Random random = new Random();
            List<String> wordPool = new ArrayList<>();
            StringBuilder word = new StringBuilder();
            for (int wordPoolSize = 0; wordPoolSize < 20000; wordPoolSize++) {
                while (true) {
                    char randomChar = charPool[random.nextInt(pool.length())];
                    if (MarkovChainService.SEPARATORS.indexOf(randomChar) == -1
                            && MarkovChainService.SPECIAL_WORDS.indexOf(randomChar) == -1)
                        word.append(randomChar);
                    else {
                        wordPool.add(word.toString());
                        word = new StringBuilder();
                        break;
                    }
                }
            }

            for (long wordCount = 0; wordCount < 50000000L; wordCount++) { //5000000000L
                fileWriter.write(wordPool.get(random.nextInt(wordPool.size())));
                fileWriter.write(MarkovChainService.SEPARATORS.toCharArray()[random.nextInt(MarkovChainService.SEPARATORS.length())]);
                if (wordCount % 5 == 0)fileWriter.write(String.valueOf(specialPool[random.nextInt(specialPool.length)])+ " ");
            }


        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error during test input file generation", e);
        }
        System.out.println("Generation finished");
    }
}